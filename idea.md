
# Objects, Commits, Mirroring

Split the game state into replicatable objects, such as chunks, entities, or
components. We can just call these _objects_ for now. There is typically some
thread, on some machine, which controls the canonical version, and others which
subscribe to it. For example, the server controls the block grid, and each 
client subscribes to it. Or, the simulation thread controls the block grid, 
and the rendering thread subscribes to it. 

![fig1](fig1.png)

Code is free to read objects, but shouldn't mutate them directly. Instead, 
create messages which describe mutations, then apply them to the objects one
after another. We can call these messages _commits_. These should be 
deterministic, so that taking an object in state A, then applying commits 
1, 2, 3, in that order, should always leave the object in state B. 

When a thread subscribes to an object, thus creating a mirrored version of it,
it opens a stream when receives 1. the entire serialized object 2. all commits
applied to that object, as they happen. By applying the commits to the mirrored
version, the mirror continues to asynchronously replicate the canonical 
version.

![fig2](fig2.png)

In most cases, commits can be composed of simple primitives like:

- Completely replace data with new data
- Apply inner commit to particular field of struct
- Apply inner commit to particular key of hashmap
- Apply inner commit to particular index of array

In some cases, it may make sense to create more logically sophisticated
commits, like "_sort this array_" or something entirely domain specific.

# Application to Concurrency

This commit-based approach lends to easy concurrency. Divide your
world-mutating operation into two steps: 

1. Read the world, potentially reading any and all objects within, and stage
   (but don't apply) commits. Since the world is read-only at this point,
   this can easily be multithreaded with parallel iterators.
2. Collect each worker thread's commit accumulator and group commits by the object
   they apply to, then apply each commit list to the relevant object. This
   can also easily by multithreaded, by dividing the objects and their relevant
   commits between worker threads, also with parallel iterators.

Or put more succinctly:

1. Stage commits
2. Apply commits

An algorithm can repeat this pattern several or many times, if necessary.
Furthermore, although the subscriber side does not perform the first step, it
can still multithread the commit application with the same strategy used in the
second step.

# Application to Information Hiding

If the server wishes to selectively hide information from clients, such as
hiding a player's inventory from other players, it must simply implement logic
to determine which clients are allowed to read which object, and refuse to
stream subscriptions to unauthorized clients.

# Integration with Client-Side Prediction

Typically, any player action which affects the shared world is 1. serialized
and sent to the server, 2. the server makes sure that the action is legal, and
3. the server mutates the world, in this case by creating commits. 

![fig3](fig3.png)

However, for certain actions like "moving forward" or "placing a block", a user
wants instantaneous feedback, and will be annoyed if the response is delayed by
at least 2 times the ping. Therefore, the client "predicts" the consequence of
the action and immediately displays it on-screen. However, the client's 
prediction may be incorrect, because another user may perform some action 
near-simultaneosly which alters the consequence of the first user's action. 

To implement client-side prediction with this architecture, a client might
create a "prediction wrapper" around the mirrored object. This wrapper would
contain a reference to the actual mirror, and a list of predicted changes. When
queried about the state of the object, the wrapper would check if that state 
was overridden, and if not, fall back to the mirror. 

The wrapper needs to have some way of knowing (or guessing) when the 
predicted change would occur, so that it can remove its predicted change at 
that time, regardless of whether the exact change did or did not canonically 
happen. 
